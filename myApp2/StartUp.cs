﻿using Microsoft.EntityFrameworkCore;

namespace myApp2
{
    public class StartUp
    {

        public IConfiguration Configuration { get; }
        public StartUp(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ManitowocDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("MyApp2ContextConnection")));

        }
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
        }
    }
}
