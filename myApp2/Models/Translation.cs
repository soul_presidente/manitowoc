﻿using System.ComponentModel.DataAnnotations.Schema;

namespace myApp2.Models
{
    [Table("FCM_Material")]
    public class Translation
    {
        public Guid Id { get; set; }
        public string LanguageId { get; set; } = "EN";
        public string Text { get; set; }

        public Translation(Guid id, string text)
        {
            Id = id;
            Text = text;
        }
    }
}
