﻿namespace myApp2.Models
{
    public class Cause
    {
        public Guid Id { get; set; }
        public string faultCode { get; set; }

        public Guid faultCodeId { get; set; }

        public string language { get; set; }
        public List<Instruction> Instructions { get; set; }
    }
}