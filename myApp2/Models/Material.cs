﻿using myApp2.Dto;
using System.ComponentModel.DataAnnotations.Schema;

namespace myApp2.Models
{

    [Table("FCM_Material")]
    public class Material
    {
        public Guid Id { get; set; }
        public bool? IsDeleted { get; set; } = false;
        public string? Code { get; set; }

        public string? RevLevel { get; set; }

        public Translation? Translation { get; set; }

        public Material(Guid id,  string? code, string? revLevel)
        {
            Id = id;
            Code = code;
            RevLevel = revLevel;
        }
    }
}
