﻿namespace myApp2.Models
{
    public class Instruction
    {
        public Guid Id { get; set; }

        public Guid CauseID { get; set; }
        public string Content { get; set; }
    }
}