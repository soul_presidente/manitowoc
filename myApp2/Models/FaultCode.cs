﻿namespace myApp2.Models
{
    public class FaultCode
    {
        public Guid Id { get; set; }
        public string Code { get; set; }

        public string language { get; set; }
        public List<Cause> Causes { get; set; } = new List<Cause>();
    }
}
