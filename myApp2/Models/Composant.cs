﻿namespace myApp2.Models
{
    public class Composant
    {
        public Guid Id { get; set; }
        public string Code { get; set; }


        public List<Cause> Causes { get; set; }
    }
}
