﻿namespace myApp2.Dto
{
    public class MaterialDto
    {
        public string? Code { get; set; }
        public string? NameTranslation { get; set; }
        public string? RevLevel { get; set; }
    }
}
