﻿using Microsoft.EntityFrameworkCore;
using myApp2.Models;

namespace myApp2
{
    public class ManitowocDbContext : DbContext
    {
        public DbSet<Material> mat { get; set; }
        public DbSet<Translation> translation { get; set; }
        public string DbPath { get; }

        public ManitowocDbContext(DbContextOptions<ManitowocDbContext> options) : base(options)
        {

        }

    }
}
