﻿using Microsoft.AspNetCore.Mvc;
using myApp2.Dto;
using myApp2.Models;
using System.Linq;
using System.Xml;

namespace myApp2.Controllers
{

    [ApiController]
    [Route("[controller]")]
    public class MaterialController : ControllerBase
    {
        private readonly ILogger<MaterialController> _logger;


        public MaterialController(ILogger<MaterialController> logger)
        {
            _logger = logger;
        }

        [HttpPost("HandleMaterial")]
        public IActionResult HandleMaterial(IFormFile file)
        {

            List<Material> list = new List<Material>();
            using (var reader = new StreamReader(file.OpenReadStream()))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(',');


                    if (values.Length > 0)
                    {
                        Material mat = new Material(Guid.NewGuid(), values[0], values[2]);
                        mat.Translation = new Translation(Guid.NewGuid(), values[1]);
                        list.Add(mat);
                    }
                }
            }

            list.RemoveAt(0);


            return Ok("ok");
        }

        [HttpPost("HandleCodeDefault")]
        public IActionResult HandleFaultCodeList(List<IFormFile> files)

        {
            List<FaultCode> FaultCodes = new List<FaultCode>();

            List<Cause> Causes = new List<Cause>();

            foreach (var file in files)
            {
                try
                {
                    HandleCodeDefault(file, FaultCodes, Causes);
                }

                catch (Exception ex)
                {
                    ex.GetBaseException();
                }

            }


            foreach (FaultCode fc in FaultCodes)
            {

                fc.Causes = Causes.Where(c => c.faultCode == fc.Code).ToList();
                fc.Causes?.ForEach(c => c.faultCodeId = fc.Id);
            }

            return Ok(FaultCodes);


        }
        private void HandleCodeDefault(IFormFile file, List<FaultCode> FaultCodes, List<Cause> Causes)
        {

            using (var reader = new StreamReader(file.OpenReadStream()))
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(reader);

                XmlNodeList IENode = doc.GetElementsByTagName("IE");

                List<XmlNode> causeNode = new List<XmlNode>();

                List<XmlNode> faultNode = new List<XmlNode>();


                for (int i = 0; i < IENode.Count; i++)
                {
                    XmlNode IENodePricipale = IENode[i];


                    var doctype = IENodePricipale.Attributes?.GetNamedItem("DOCTYPE")?.Value;
                    var language = IENodePricipale.Attributes?.GetNamedItem("MADDR-LANGUAGE-ORG")?.Value;

                    if (doctype == "FAULT")
                    {
                        faultNode.Add(IENode[i]);
                        var faultCode = IENodePricipale.Attributes?.GetNamedItem("MADDR-ANCHOR")?.Value;
                        
                        FaultCode fc = new FaultCode();
                        fc.Id = Guid.NewGuid();
                        fc.language = language;
                        fc.Code = faultCode?.ToString();

                      
                         if(!FaultCodes.Exists(fc => fc.Code == faultCode))
                        {
                            FaultCodes.Add(fc);
                        }
                        

                    }
                    else if (doctype == "CAUSE")
                    {
                        var tag = IENodePricipale.FirstChild;
                        var tagContent = tag?.FirstChild;

                        var tagVerification = tagContent?.SelectNodes("TAG")
                                                       .Cast<XmlNode>()
                                                       .FirstOrDefault(n => n.Attributes["NAME"].Value == "VERIFICATION");


                        var instructionsNodes = FindInstructionNodes(tagVerification);

                        causeNode.Add(IENode[i]);

                        var faultCode = IENodePricipale.Attributes?.GetNamedItem("MADDR-ANCHOR")?.Value;

                        Cause c = new Cause();
                        c.Id = Guid.NewGuid();
                        c.faultCode = faultCode?.ToString();
                        c.language = language;
                        c.Instructions = new List<Instruction>();
                        FindInstruction(instructionsNodes, c);

                        Causes.Add(c);

                    }

                }

            }


        }
        private List<XmlNode> FindInstructionNodes(XmlNode node, List<XmlNode> nodes = null)
        {
            if (nodes == null)
                nodes = new List<XmlNode>();

            if (node.Attributes?["NAME"]?.Value == "INSTRUCTION")
            {
                nodes.Add(node);
            }

            if (node.HasChildNodes)
            {
                foreach (XmlNode child in node.ChildNodes)
                {
                    FindInstructionNodes(child, nodes);
                }

            }

            return nodes;
        }
        private void FindInstruction(List<XmlNode> nodes, Cause Cause)
        {
            foreach (XmlNode node in nodes)
            {
                Instruction instruction = new Instruction();
                instruction.Id = Guid.NewGuid();
                instruction.CauseID = Cause.Id;
                instruction.Content = node?.SelectSingleNode("TAG-CONTENT")?.SelectSingleNode("DATA")?.InnerText;
                Cause.Instructions.Add(instruction);
            }

        }

        [HttpPost("CouplesComposantsDefaut")]
        public IActionResult CouplesComposantsDefauts(List<IFormFile> files)
        {

            List<Composant> Composants = new List<Composant>();
            List<Cause> Causes = new List<Cause>();
            foreach (var file in files)
            {
                using (var reader = new StreamReader(file.OpenReadStream()))
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(reader);
                    XmlNodeList IENode = doc.GetElementsByTagName("IE");
                    List<XmlNode> composantNode = new List<XmlNode>();
                   
                    List<XmlNode> defautNode = new List<XmlNode>();


                    List<string> attributeNumValues = new List<string>();

                    
                    for (int i = 0; i < IENode.Count; i++)
                    {
                        XmlNode IENodePricipale = IENode[i];
                        var doctype = IENodePricipale.Attributes?.GetNamedItem("DOCTYPE")?.Value;
                        if (doctype == "OBJECT")
                        {
                            composantNode.Add(IENode[i]);
                            var composant = IENodePricipale.Attributes?.GetNamedItem("MADDR-ANCHOR")?.Value;
                            Composant comp = new Composant();
                            comp.Code = composant?.ToString();
                            Composants.Add(comp);
                        }
                        else if (doctype == "CAUSE")
                        {
                            var tag = IENodePricipale.FirstChild;


                            var dataAttribute  = tag.Attributes?.GetNamedItem("DATTR1")?.Value;

                            var attributeNumValue = dataAttribute?.Split('_') ?[0];



                           // attributeNumValues.Add(attributeNumValue);

                            Cause c = new Cause();
                            c.faultCode = attributeNumValue.ToString();
                            
                            Causes.Add(c);



                        }
                    }

                   
                }

                

            }


            foreach (Composant cp in Composants)
            {

                cp.Causes = Causes.Where(c => c.faultCode == cp.Code).ToList();
                //  cp.Causes?.ForEach(c => c.faultCodeId = fc.Id);
            }

            return Ok(Composants);
        }


        [HttpPost("GetV3")]
        public IActionResult GetV3(List<IFormFile> files)
        {
            List<ComponsantV3> componsantV3s = new List<ComponsantV3>();
            foreach (var file in files)
            {

                using (var reader = new StreamReader(file.OpenReadStream()))

                {
                    XmlDocument doc = new XmlDocument();
                    //XmlNodeList xmlnode;
                    doc.Load(reader);
                    XmlNodeList IENode = doc.GetElementsByTagName("IE");
                    for (int i = 0; i < IENode.Count; i++)
                    {
                        var componsantV3 = new ComponsantV3();
                        XmlNode node = IENode[i];
                        var tagObject = node.SelectNodes("TAG")[0];
                        var tagObjectContent = tagObject.SelectNodes("TAG-CONTENT")[0];
                        var tags = tagObjectContent.SelectNodes("TAG");

                        foreach (XmlNode tag in tags)
                        {
                            switch (tag.Attributes["NAME"].InnerText)
                            {
                                case "NAME":
                                    var tagObjectContent2 = tag.SelectNodes("TAG-CONTENT")[0];
                                    componsantV3.Name = tagObjectContent2.SelectNodes("DATA")[0].InnerText;
                                    break;
                                case "GRP-ERES-ID":

                                    var tagObjectContentX = tag.SelectNodes("TAG-CONTENT")[0];
                                    var tagObjectEresID = tagObjectContentX.SelectNodes("TAG")[0];
                                    var tagEresID = tagObjectEresID.SelectNodes("TAG-CONTENT")[0];
                                    var tagID = tagEresID.SelectNodes("DATA")[0];
                                    componsantV3.EresId = tagID.SelectNodes("NO-TRANS")[0].InnerText;
                                    break;
                                default:
                                    break;
                            }


                        }
                        // componsantV3.Name = tag.Attributes["DATTR1"].InnerText;
                        componsantV3s.Add(componsantV3);
                    }
                }
            }

            return Ok(componsantV3s);
        }
    }

    public class ComponsantV3
    {
        public string Name { get; set; }
        public string EresId { get; set; }
    }

}


